const db = require('../config/db.config');
const Ville = db.Ville;
const { Op } = require("sequelize");

exports.getVilles = (req, res) => {
  try {
    const page = parseInt(req.query.page);
    const limit = parseInt(req.query.limit);
    const departement = parseInt(req.query.departement);
    const nameVille = req.query.ville !== undefined ? req.query.ville : '';
    const sort = req.query.sort;
    const popSorte = req.query.popSorte
  
    const offset = page ? page * limit : 0;

    return Ville.findAndCountAll({
      where: departement ? {[Op.and]: [{ ville_departement: departement }, { ville_nom: { [Op.like]: nameVille  + '%'} }]} : { ville_nom: { [Op.like]: nameVille  + '%'} },
      order: (sort && popSorte) ? [['ville_population_' + popSorte, sort]] : [],
      limit: limit, 
      offset: offset
    }).then(data => {
        const totalPages = Math.ceil(data.count / limit);
        const response = {
          data: {
              "totalItems": data.count,
              "totalPages": totalPages,
              "limit": limit,
              "departement-filtering": departement,
              "currentPageNumber": page + 1,
              "currentPageSize": data.rows.length,
              "villes": data.rows
          }
        };
        res.send(response);
      });  
  } catch(error) {
      res.status(500).send({
        message: "Error -> Can NOT complete a paging request!",
        error: error.message,
      });
  }      
} 