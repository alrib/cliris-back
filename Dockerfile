FROM node:12

RUN mkdir -p /api
WORKDIR /api

COPY package.json /api

COPY . /api

RUN npm install

CMD ["npm", "start"]