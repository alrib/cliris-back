module.exports = (sequelize, Sequelize) => {
    const Ville = sequelize.define('villes_france_free', {	
        ville_id: {
            type: Sequelize.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true
        },
	    ville_departement: {
		    type: Sequelize.STRING
	    },
	    ville_slug: {
            type: Sequelize.STRING
	    },	 
        ville_nom: {
			type: Sequelize.STRING
	    },
	    ville_nom_simple: {
			type: Sequelize.STRING
        },
        ville_nom_reel: {
            type: Sequelize.STRING,
        },
        ville_nom_soundex: {
            type: Sequelize.STRING,
        },
        ville_nom_metaphone: {
            type: Sequelize.STRING,
        },
        ville_code_postal: {
            type: Sequelize.STRING,
        },
        ville_commune: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true
        },
        ville_code_commune: {
            type: Sequelize.STRING,
        },
        ville_arrondissement: {
            type: Sequelize.INTEGER.UNSIGNED,
        },
        ville_canton: {
            type: Sequelize.STRING,
        },
        ville_amdi: {
            type: Sequelize.INTEGER.UNSIGNED,
        },
        ville_population_2010: {
            type: Sequelize.INTEGER.UNSIGNED,
        },
        ville_population_1999: {
            type: Sequelize.INTEGER.UNSIGNED,
        },
        ville_population_2012: {
            type: Sequelize.INTEGER.UNSIGNED,
        },
        ville_densite_2010: {
            type: Sequelize.INTEGER,
        },
        ville_surface: {
            type: Sequelize.FLOAT,
        },
        ville_longitude_deg: {
            type: Sequelize.FLOAT,
        },
        ville_latitude_deg: {
            type: Sequelize.FLOAT,
        },
        ville_longitude_grd: {
            type: Sequelize.STRING,
        },
        ville_latitude_grd: {
            type: Sequelize.STRING,
        },
        ville_longitude_dms: {
            type: Sequelize.STRING,
        },
        ville_latitude_dms: {
            type: Sequelize.STRING,
        },
        ville_zmin: {
            type: Sequelize.INTEGER,
        },
        ville_zmax: {
            type: Sequelize.INTEGER,
        },
	},  
    { 
        sequelize, 
        modelName: 'VillesFranceFree', 
        tableName: 'villes_france_free',
        createdAt: 'date_created',
        underscore: true,
        timestamps: false,
    });

	return Ville;
}