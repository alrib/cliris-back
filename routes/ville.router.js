const express = require('express');
const router = express.Router();

const villeCtrl = require('../controllers/ville.controller');

router.get('/api/villes', villeCtrl.getVilles); 

module.exports = router;