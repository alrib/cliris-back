process.env.NODE_ENV = "test"

const Ville = require('../models/ville.model').Ville;
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../server');

const should = chai.should();

chai.use(chaiHttp)

describe('/GET villes', () => {
    it('it should Get all villes', (done) => {
        chai.request(app)
        .get('/api/villes').query({limit: 20})
        .then(res => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            done();
        }).catch(done);
    });
});
